package com.cc3002.breakout.facade;

import com.cc3002.breakout.logic.IBrick;
import com.cc3002.breakout.logic.ILevel;
import com.cc3002.breakout.logic.Juego;
import com.cc3002.breakout.logic.Level;

import java.util.List;

/**Recopila los metodos para interactuar  on el juego. */
public class HomeworkOneFacade {
  public Juego juego = new Juego();

  public ILevel newLevelWithSoftAndStoneBricks(String levelName, int number, double probability) {
    return new Level(levelName, number, probability, juego);
  }

  public long numberOfBricks() {
    return getCurrentLevel().numLadrillos();
  }
  
  public List<IBrick> getBricks() {
    return juego.nivelActual().getBricks();
  }

  public boolean hasNextLevel() {
    return !juego.lastLevel();
  }

  public String getLevelName() {
    return juego.nombreNivel();
  }

  public double getRequiredPoints() {
    return juego.nivelActual().puntajeRequerido();
  }

  public int getNumberOfHearts() {
    return juego.numeroVidas();
  }

  public int lossOfHeart() {
    juego.perderVida();
    return getNumberOfHearts();
  }

  public long earnedScore() {
    return juego.puntajeGanado();
  }

  public ILevel getCurrentLevel() {
    return juego.nivelActual();
  }

  public void setCurrentLevel(final ILevel newLevel) {
    juego.setNivel(newLevel);
  }

  public String spawnBricks(final ILevel level) {
    return juego.imprimirNivel(level);
  }
}
