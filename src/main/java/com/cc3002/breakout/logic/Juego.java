package com.cc3002.breakout.logic;

import java.util.List;

/** Inicializa el juego con los parametros que determinan el estado actual del juego. */
public class Juego {
  private ILevel nivel;
  private final boolean ultimoNivel;
  private int vidas;
  private long puntajeJuego;

  public Juego() {
    vidas = 3;
    ultimoNivel = false;
  }
 
  public ILevel nivelActual() {
    return nivel;
  }

  public boolean lastLevel() {
    return ultimoNivel;
  }

  public String nombreNivel() {
    return nivel.nombre();
  }

  public int numeroVidas() {
    return vidas;
  }

  public void perderVida() {
    vidas--;
  }

  public void setNivel(final ILevel newLevel) {
    nivel = newLevel;
  }
  
  public void setPuntaje(long puntos) {
    puntajeJuego = puntos;
  }
  
  public long puntajeGanado() {
    return puntajeJuego;
  }

  /** Retorna un String que representa los ladrillos del nivel. */
  public String imprimirNivel(ILevel level) {
    StringBuilder sb = new StringBuilder();
    List<IBrick> ladrillos = level.getBricks();
    int contador = 0;
    for (int k = level.numLadrillos() - 1; k >= 0; k--) {
      sb.insert(0, ladrillos.get(k).toChar());
      contador++;
      if (contador % 16 == 0 && contador > 0) {
        sb.insert(0, System.lineSeparator());
      }
    }
    return sb.toString();
  }
}
