package com.cc3002.breakout.logic;

import java.util.List;

/** Interfaz para interactuar con clase Level.*/
public interface ILevel {
  public int numLadrillos();

  public List<IBrick> getBricks();

  public String nombre();

  public long puntajeNivel();

  public long puntajeRequerido();

  public void setPuntaje(long puntos);
  
  public Juego getJuego();
}
