package com.cc3002.breakout.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/** Representa un nivel del juego con los ladrillos y un puntaje maximo y uno actual. */
public class Level implements ILevel {
  private final String nombreNivel;
  private final int numeroLadrillos;
  private final List<IBrick> ladrillos;
  private final long puntajeMaximo;
  private long puntajeNivel;
  private final Juego juego;
  
  /** Constructor de la clase Level que guarda referencia al juego al que pertenece. */
  public Level(String levelName, int number, double probability, Juego game) {
    this.nombreNivel = levelName;
    juego = game;
    numeroLadrillos = number;
    long puntos = 0;
    puntajeNivel = 0;
    ladrillos = new ArrayList<IBrick>();
    Random numeroRandom = new Random();
    for (int i = 0; i < number; i++) {
      double pivote = numeroRandom.nextDouble();
      if (pivote <= probability) {
        puntos += 10;
        IBrick brick = new SoftBrick(this);
        this.ladrillos.add(brick);
      } else {
        puntos += 50;
        IBrick brick = new StoneBrick(this);
        this.ladrillos.add(brick);
      }
    }
    puntajeMaximo = puntos;
  }

  public int numLadrillos() {
    return numeroLadrillos;
  }

  public List<IBrick> getBricks() {
    return ladrillos;
  }

  public String nombre() {
    return this.nombreNivel;
  }

  public long puntajeRequerido() {
    Double pm = puntajeMaximo * 0.7;
    return pm.intValue();
  }

  public long puntajeNivel() {
    return puntajeNivel;
  }

  public void setPuntaje(long puntos2) {
    puntajeNivel = puntos2;
  }
  
  public Juego getJuego() {
    return juego;
  }
}
