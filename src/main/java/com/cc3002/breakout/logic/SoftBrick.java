package com.cc3002.breakout.logic;

/** Crea un ladrillo suave que resiste 1 golpe. */
public class SoftBrick implements IBrick {
  private int hits;
  private final ILevel level;

  public SoftBrick(ILevel nivel) {
    hits = 1;
    level = nivel;
  }

  public boolean isSoftBrick() {
    return true;
  }

  public boolean isStoneBrick() {
    return false;
  }

  public int remainingHits() {
    return hits;
  }

  /** Disminuye los hits del ladrillo y suma el puntaje correspondiente si se quiebra. */
  public boolean hit() {
    this.hits--;
    if (this.hits == 0) {
      level.setPuntaje(level.puntajeNivel() + 10);
      Juego game = level.getJuego();
      game.setPuntaje(game.puntajeGanado() + 10);
      return true;
    } else {
      return false;
    }
  }

  public char toChar() {
    return '*';
  }

  public boolean equals(IBrick brick) {
    return brick.equals(this);
  }

  public boolean equals(SoftBrick sb1) {
    return hits == sb1.remainingHits();
  }

  public boolean equals(StoneBrick sb2) {
    return false;
  }
}
