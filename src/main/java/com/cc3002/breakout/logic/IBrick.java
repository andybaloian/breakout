package com.cc3002.breakout.logic;

/** Interfaz para interactuar con ladrillos tipo SoftBrick y StoneBrick.*/
public interface IBrick {
  public boolean isSoftBrick();

  public boolean isStoneBrick();

  public int remainingHits();

  public boolean hit();

  public char toChar();

  public boolean equals(IBrick b1);

  public boolean equals(SoftBrick b2);

  public boolean equals(StoneBrick b3);
}
