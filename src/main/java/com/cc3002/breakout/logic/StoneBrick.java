package com.cc3002.breakout.logic;

/** Crea un ladrillo de piedra que resiste 3 golpes. */
public class StoneBrick implements IBrick {
  private int hits;
  private final ILevel level;

  public StoneBrick(ILevel nivel) {
    hits = 3;
    level = nivel;
  }

  public boolean isSoftBrick() {
    return false;
  }

  public boolean isStoneBrick() {
    return true;
  }

  public int remainingHits() {
    return hits;
  }
  
  /** Disminuye los hits del ladrillo y suma el puntaje correspondiente si se quiebra. */
  public boolean hit() {
    this.hits--;
    if (this.hits == 0) {
      level.setPuntaje(level.puntajeNivel() + 50);
      Juego game = level.getJuego();
      game.setPuntaje(game.puntajeGanado() + 50);
      return true;
    } else {
      return false;
    }
  }

  public char toChar() {
    return '#';
  }

  public boolean equals(IBrick brick) {
    return brick.equals(this);
  }

  public boolean equals(StoneBrick sb1) {
    return this.hits == sb1.remainingHits(); 
  }

  public boolean equals(SoftBrick sb2) {
    return false;
  }
}
