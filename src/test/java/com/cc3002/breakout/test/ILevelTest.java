package com.cc3002.breakout.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.*;

import com.cc3002.breakout.logic.IBrick;
import com.cc3002.breakout.logic.ILevel;
import com.cc3002.breakout.logic.Juego;
import com.cc3002.breakout.logic.Level;

public class ILevelTest {
  private ILevel nivel1;
  private ILevel nivel2;

  @Before
  public void setUp() {
    Juego game = new Juego();
    nivel1 = new Level("nivel1", 16, 0.5, game);
    nivel2 = new Level("nivel2", 16, 0.5, game);
  }

  @Test
  public void testEquals() {
    assertNotNull(nivel1);
    assertNotNull(nivel2);
    assertNotEquals(nivel1, nivel2);
    assertEquals(nivel1, nivel1);
    assertEquals(nivel2, nivel2);
  }

  @Test
  public void testMetodos() {
    assertTrue(nivel1.puntajeNivel() == 0);
    List<IBrick> lista = nivel1.getBricks();
    int largoLista = lista.size();
    long puntosMax = 0;
    for (int indice = 0; indice < largoLista; indice++) {
      IBrick brick = lista.get(indice);
      if (brick.isSoftBrick()) {
        puntosMax = puntosMax + 10;
      } else {
        puntosMax = puntosMax + 50;
      }
    }
    assertTrue(nivel1.puntajeRequerido() == puntosMax * 0.7);
    nivel1.setPuntaje(549);
    assertTrue(549 == nivel1.puntajeNivel());
    assertTrue(nivel1.numLadrillos() == largoLista);
  }

}
