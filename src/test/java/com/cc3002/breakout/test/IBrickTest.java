package com.cc3002.breakout.test;

import static org.junit.Assert.*;
import org.junit.*;

import com.cc3002.breakout.logic.IBrick;
import com.cc3002.breakout.logic.ILevel;
import com.cc3002.breakout.logic.Juego;
import com.cc3002.breakout.logic.Level;
import com.cc3002.breakout.logic.SoftBrick;
import com.cc3002.breakout.logic.StoneBrick;

public class IBrickTest {
  private IBrick b1;
  private IBrick b2;
  private IBrick b3;
  private IBrick b4;
  private SoftBrick b5;
  private StoneBrick b6;
  private SoftBrick b7;
  private StoneBrick b8;

  @Before
  public void setUp() {
    Juego game = new Juego();
    ILevel level = new Level("testLevel", 0, 0, game);
    b1 = new SoftBrick(level);
    b2 = new StoneBrick(level);
    b3 = new SoftBrick(level);
    b4 = new StoneBrick(level);
    b5 = new SoftBrick(level);
    b6 = new StoneBrick(level);
    b7 = new SoftBrick(level);
    b8 = new StoneBrick(level);
  }

  @Test
  public void testEquals() {
    assertNotNull(b1);
    assertNotEquals(b1, b2);
    assertEquals(b1, b1);
    assertNotEquals(b2, b4);
    assertNotEquals(b1, b3);
    assertTrue(b1.equals(b5));
    assertFalse(b1.equals(b6));
    assertTrue(b2.equals(b6));
    assertFalse(b2.equals(b5));
    assertTrue(b2.equals(b6) == true);
    assertTrue(b1.equals(b5) == true);
    assertFalse(b2.equals(b5) == true);
    assertFalse(b1.equals(b6) == true);
    assertTrue(b5.equals(b7));
    assertTrue(b6.equals(b8));
  }

  @Test
  public void testHit() {
    assertTrue(b1.hit());
    assertFalse(b2.hit());
    assertFalse(b2.hit());
    assertTrue(b2.hit());
  }

  @Test
  public void testHitpoints() {
    assertTrue(b1.remainingHits() == 1);
    assertTrue(b2.remainingHits() == 3);
    b2.hit();
    b2.hit();
    assertTrue(b2.remainingHits() == 1);
    assertTrue(b1.remainingHits() == 1);
    assertTrue(b1.hit());
    assertTrue(b1.remainingHits() == 0);
  }

  @Test
  public void brickTypeTest() {
    assertTrue(b1.isSoftBrick());
    assertFalse(b1.isStoneBrick());
    assertTrue(b2.isStoneBrick());
    assertFalse(b2.isSoftBrick());
  }

  @Test
  public void brickCharTest() {
    assertTrue(b1.toChar() == '*');
    assertTrue(b2.toChar() == '#');
  }

  @Test
  public void equalsOverloadTest() {
    assertTrue(b1.equals(b3));
    assertTrue(b2.equals(b4));
    assertFalse(b1.equals(b2));
    assertFalse(b3.equals(b4));
  }
}
