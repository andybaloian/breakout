package com.cc3002.breakout.test;

import static org.junit.Assert.*;
import org.junit.*;

import com.cc3002.breakout.facade.HomeworkOneFacade;
import com.cc3002.breakout.logic.IBrick;

public class FacadeTest {
  private HomeworkOneFacade prueba;
  private HomeworkOneFacade prueba2;
  
  @Before
  public void setUp() {
    prueba = new HomeworkOneFacade();
    prueba2 = new HomeworkOneFacade();
  }
  
  @Test
  public void testMetodos() {
    assertNull(prueba.getCurrentLevel());
    assertTrue(prueba.hasNextLevel());
    prueba.setCurrentLevel(prueba.newLevelWithSoftAndStoneBricks("nuevoNivel", 16, 0.5));
    assertNotNull(prueba.getCurrentLevel());
    assertEquals(prueba.numberOfBricks(), prueba.getCurrentLevel().numLadrillos());
    assertTrue(prueba.hasNextLevel());
    assertTrue(prueba.hasNextLevel() != prueba.getCurrentLevel().getJuego().lastLevel());
    assertEquals(prueba.hasNextLevel(), prueba2.hasNextLevel());
    assertEquals(prueba.getLevelName(), "nuevoNivel");
    assertTrue(prueba.getRequiredPoints() == prueba.getCurrentLevel().puntajeRequerido());
    int corazones = prueba.getNumberOfHearts();
    prueba.lossOfHeart();
    assertEquals(corazones, prueba.getNumberOfHearts() + 1);
    assertEquals(prueba.earnedScore(), 0);
    IBrick brick = prueba.getBricks().get(0);
    brick.hit();
    brick.hit();
    brick.hit();
    assertTrue(prueba.earnedScore() > 0);
    assertTrue(prueba.spawnBricks(prueba.getCurrentLevel()) instanceof String);
    assertTrue(prueba.getCurrentLevel().getJuego().imprimirNivel(prueba.getCurrentLevel()).length() == prueba.getCurrentLevel().getBricks().size() + 2 * prueba.getCurrentLevel().getBricks().size() / 17 );
    
  }
}
